\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern, textcomp}% http://www.tug.dk/FontCatalogue/lmodern/
\usepackage[
  style= numeric,
  sorting= none,
  giveninits= true,
%  terseinits= true, % no period after initial
  natbib= true,
  maxnames= 99,
  isbn= false,
  eprint= false,
%  defernumbers= true, % numbers both sections separately, i.e. starting with 1
  backend= biber
]{biblatex}
\addbibresource{WBIR2018-poster.bib}
\DeclareNameAlias{default}{last-first}% https://tex.stackexchange.com/questions/290103/get-last-name-first-in-biblatex-using-numeric-style#comment702153_290104
\DeclareSourcemap{% https://tex.stackexchange.com/questions/76534/biblatex-print-isbn-only-if-doi-is-not-defined#76542
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=doi,final]
      \step[fieldset=url,null]
    }
  }
}

\renewcommand*{\bibfont}{\tiny}% http://tex.stackexchange.com/questions/329/how-to-change-font-size-for-bibliography

\setbeamertemplate{bibliography item}{% http://tex.stackexchange.com/questions/68080/beamer-bibliography-icon#68084
  \ifboolexpr{%
    test {\ifentrytype{book}} or test {\ifentrytype{article}} or test {\ifentrytype{inproceedings}} or test {\ifentrytype{incollection}}
  }
    {\setbeamertemplate{bibliography item}[text]}% text <=> no icon but #
    {\ifentrytype{online}
       {\setbeamertemplate{bibliography item}[online]}% online, book, article <=> icon
       {\setbeamertemplate{bibliography item}[triangle]}% triangle <=> symbol
    }%
  \usebeamertemplate{bibliography item}
}

\usepackage{ragged2e}% justification for beamer: https://tex.stackexchange.com/questions/55589/text-justify-in-beamer#55590
\usepackage{calc}% subtraction of lengths

%***********include SVG-files ;-)))
\usepackage{transparent} %for transparent text in inkscape files
\usepackage{import}                 %for svg commands

\newcommand{\executeiffilenewer}[3]{%
  \ifnum\pdfstrcmp{\pdffilemoddate{#1}}%
  {\pdffilemoddate{#2}}>0%
  {\immediate\write18{#3}}\fi%
}
\newcommand{\includesvg}[2]{%
  \executeiffilenewer{#1/#2.svg}{#1/#2.pdf_tex}%  
  {inkscape -z --file=#1/#2.svg --export-pdf=#1/#2.pdf --export-latex --export-area-page}%--export-area-drawing
  \import{#1/}{#2.pdf_tex}%
}%
%***********include SVG-files ;-)))

%\setbeamercolor{title}{fg=black}

\title[]{Enabling manual intervention for otherwise\\automated registration of large image series}
\author[Grothausmann \& Knudsen \& Ochs \& Mühlfeld]{Grothausmann R., Knudsen L., Ochs M., Mühlfeld C.}
\institute[Hannover Medical School]{Institute of Functional and Applied Anatomy, Hannover Medical School}
\date{Jun. 29th, 2018}

\newlength{\hpwidth}

\renewcommand{\UrlBreaks}{\do\?\do\&\do\-\do\.\do\:}

\begin{document}


\frame{\titlepage}

\begin{frame}{The task}
  \begin{itemize}
  \item Aligning a few 1000 images of lung tissue (serial sectioning)
  \item Tools such as Fiji's "Register Virtual Stack Slices"~\cite{fiji:virtual-stack-reg} work well up to a few 100 images but then finding suitable meta-parameters becomes very time consuming
  \item Developed program suite using SimpleElastix~\cite{elastix, simpleelastix} to enable manual intervention for otherwise automated registration
  \end{itemize}
  
  \begin{figure}
    \setlength{\hpwidth}{1.0\textwidth}
    \begin{center}
      \tiny%
      \def\svgwidth{\hpwidth}%
      \includesvg{figs/}{MR3006g_OT2_b+64}% make in slides2stack/
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{The challenges}
  \begin{itemize}
  \item Alignment of lung tissue images suffers from local minima due to the sparse tissue and its similar, repetitive structure
  \item Physical cutting can lead to shear and local deformations, differing even for successive sections
  \end{itemize}
  
  \begin{figure}
    \setlength{\hpwidth}{0.32\textwidth}
    \includegraphics[width=\hpwidth]{figs/000007_17-296_0092_s00_01.png}%
    \hfill
    \includegraphics[width=\hpwidth]{figs/000008_17-296_0092_s01_01.png}%
    \hfill
    \includegraphics[width=\hpwidth]{figs/17-296_0090_7+8.png}%
  \end{figure}
\end{frame}

\begin{frame}{Looking for Solutions}
  \begin{enumerate}
  \item Reduction of the need for manual interactions with e.g. optimization of default meta-parameters
  \item Local deformation registration with/without a ``guide tomogram''
  \item Number of threads influences metric value:\\ leads to instabilities after a few 100 images
  \end{enumerate}
  
  \begin{figure}
    \setlength{\hpwidth}{\textwidth}
    \includegraphics[width=\hpwidth]{figs/K2_reg_smROI.png}%
  \end{figure}
\end{frame}

\begin{frame}{Thanks}
  for coding, support, maintenance and help go to:
  
  \begin{itemize}
  \item Kasper Marstal% https://github.com/kaspermarstal
  \item Marius Staring% https://github.com/mstaring
  \item Fabien Pertuy% https://github.com/PertuyF
  \item Stefan Klein% https://github.com/stefanklein
  \item Elastix and ITK community
  \end{itemize}
  \vspace{-20mm}

  \begin{figure}
    \setlength{\hpwidth}{0.49\textwidth}
    \hfill
    \includegraphics[width=\hpwidth]{h3d-article/images/fig_ov01/fig_ov01_4.png}%
    \hspace{-5mm}
    \vspace{-5mm}
    \llap{\makebox[\hpwidth][r]{\def\svgwidth{0.1\hpwidth}\includesvg{figs/}{qrcode}}}% https://tex.stackexchange.com/questions/89776/image-inside-another-image#89778
  \end{figure}
\end{frame}

\begin{frame}{References}
  \RaggedRight% https://tex.stackexchange.com/questions/403429/force-linebreak-in-url-with-beamer-biblatex-and-biber#403454
  \printbibliography
\end{frame}


\end{document}
