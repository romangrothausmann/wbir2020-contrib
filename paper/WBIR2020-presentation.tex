\documentclass{beamer}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern, textcomp}% http://www.tug.dk/FontCatalogue/lmodern/
\usepackage[
  style= numeric,
  sorting= none,
  giveninits= true,
%  terseinits= true, % no period after initial
  natbib= true,
  maxnames= 99,
  isbn= false,
  eprint= false,
%  defernumbers= true, % numbers both sections separately, i.e. starting with 1
  backend= biber
]{biblatex}
\addbibresource{WBIR2020-presentation.bib}
\DeclareNameAlias{default}{last-first}% https://tex.stackexchange.com/questions/290103/get-last-name-first-in-biblatex-using-numeric-style#comment702153_290104
\DeclareSourcemap{% https://tex.stackexchange.com/questions/76534/biblatex-print-isbn-only-if-doi-is-not-defined#76542
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=doi,final]
      \step[fieldset=url,null]
    }
  }
}

\usepackage{tikz}
\usepackage{siunitx}
\newcommand{\scalebar}[5][white]{% https://gist.github.com/rbnvrw/00312251b756f6b48084#scalebar-without-background % https://tex.stackexchange.com/questions/67815/how-to-add-a-scalebar-to-an-image
 \begin{tikzpicture}
  \draw (0,0) node[anchor=south west,inner sep=0] (image) { #2 };
  \begin{scope}[x={(image.south east)},y={(image.north west)}]
   \draw [#1, line width=0.2em] (0.04,1.2em) -- node[below,inner sep=0.1em, font=\footnotesize] {\SI{#5}{\micro \meter}} (#5*#4/#3+0.04,1.2em);
  \end{scope}
 \end{tikzpicture}
}

\renewcommand*{\bibfont}{\tiny}% http://tex.stackexchange.com/questions/329/how-to-change-font-size-for-bibliography

\setbeamertemplate{bibliography item}{% http://tex.stackexchange.com/questions/68080/beamer-bibliography-icon#68084
  \ifboolexpr{%
    test {\ifentrytype{book}} or test {\ifentrytype{article}} or test {\ifentrytype{inproceedings}} or test {\ifentrytype{incollection}}
  }
    {\setbeamertemplate{bibliography item}[text]}% text <=> no icon but #
    {\ifentrytype{online}
       {\setbeamertemplate{bibliography item}[online]}% online, book, article <=> icon
       {\setbeamertemplate{bibliography item}[triangle]}% triangle <=> symbol
    }%
  \usebeamertemplate{bibliography item}
}

\usepackage{ragged2e}% justification for beamer: https://tex.stackexchange.com/questions/55589/text-justify-in-beamer#55590
\usepackage{calc}% subtraction of lengths

%***********include SVG-files ;-)))
\usepackage{transparent} %for transparent text in inkscape files
\usepackage{import}                 %for svg commands

\newcommand{\executeiffilenewer}[3]{%
  \ifnum\pdfstrcmp{\pdffilemoddate{#1}}%
  {\pdffilemoddate{#2}}>0%
  {\immediate\write18{#3}}\fi%
}
\newcommand{\includesvg}[2]{%
  \executeiffilenewer{#1/#2.svg}{#1/#2.pdf_tex}%  
  {inkscape -z --file=#1/#2.svg --export-pdf=#1/#2.pdf --export-latex --export-area-page}%--export-area-drawing
  \import{#1/}{#2.pdf_tex}%
}%
%***********include SVG-files ;-)))

%\setbeamercolor{title}{fg=black}

\title[]{Enabling manual intervention for otherwise\\automated registration of large image series}
\author{Roman Grothausmann \& Dženan Zukić \& Matt McCormick \& Christian Mühlfeld \& Lars Knudsen}
\institute[Hannover Medical School]{
  Institute of Functional and Applied Anatomy, Hannover Medical School, \\
  Faculty of Engineering and Health, HAWK University of Applied Sciences and Arts
}
\date{Dec. 2nd, 2020}

\newlength{\hpwidth}

\renewcommand{\UrlBreaks}{\do\?\do\&\do\-\do\.\do\:}

\begin{document}


\frame{\titlepage}

\begin{frame}{The task}
  \begin{itemize}[<+->]
  \item Ca. 2600 images of 1~µm thick serial sections of lung tissue
  \item Cutting and histol. processing misaligns and distorts sections
  \item Registration needed to reconstruct spatial correspondence of 3rd dimension to obtain a dataset for 3D analyses
  \end{itemize}
  
  \begin{figure}
    \begin{center}
      \begin{tabular}{c|c|c|c|c}
        \scalebox{0.5}{\rotatebox{90}{\includegraphics{figs/K2_0008_s02.png}}} &
        \scalebox{0.5}{\rotatebox{90}{\includegraphics{figs/K2_0006_s00.png}}} &
        \scalebox{0.5}{\rotatebox{90}{\scalebar[black]{\includegraphics{figs/K2_0105_s05.png}}{763}{0.018}{10000}}} &
        \scalebox{0.5}{\rotatebox{90}{\includegraphics{figs/K2_0026_s00.png}}} &
        \scalebox{0.5}{\rotatebox{90}{\includegraphics{figs/K2_0105_s02.png}}}
      \end{tabular}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{The Problem}
  \begin{itemize}[<+->]
  \item Registration of lung tissue images suffers from local minima due to the sparse tissue and its similar, repetitive structure
  \item Tools such as Fiji's "Register Virtual Stack Slices"~\cite{fiji:virtual-stack-reg} work well up to a few 100 images
  \item But then finding suitable meta-parameters becomes very time consuming
  \item Developed \texttt{recRegStack.py}\footnote<.->{\url{http://github.com/romangrothausmann/elastix_scripts/}} program based on SimpleElastix~\cite{elastix, simpleelastix}
  \item \texttt{recRegStack.py} enables manual intervention for otherwise automated registration
  \end{itemize}
\end{frame}

\begin{frame}{Dependencies of the iterative process}
  \begin{columns}[b]
  \column{.5\textwidth}
  \begin{itemize}[<+->]
  \item Images are represented by squares, text-files by ovals.
  \item Default parameters (dfl.~PF, dPF) for the registration (reg.) need to be tuned for the image series (2D~series) to reduce the need for manual interactions.
%    The first image at the start is copied unchanged. The last aligned image (ali.~Img) is used as fixed image (fix.~Img, fI) for registering the next image from the series (mov.~Img, mI).
  \item In case the result of dPF is unacceptable, provide a manual initial transform (man.~iT, mIT) and/or individual registration parameters (idv.~PF, iPF).
%    Tuning the default parameters (dPF) is the most difficult (time consuming, red) task, adjusting some individual parameters less problematic (iPF, yellow) while creating a manual initial transform (mIT, green) with e.g. \texttt{midas} is easiest.
%  \item In case some images need to be re-scanned (due to scan-artefacts, defocus, etc), the transform parameter files (tra.~PF, tPF) can be used to register the new image exactly the same way (reprod.) or the registration process can be re-initiated to make use of the improved image quality.
  \end{itemize}
  \column{.5\textwidth}
  \begin{figure}
    \setlength{\hpwidth}{1.1\textwidth}
    \begin{center}
      \tiny%
      \def\svgwidth{\hpwidth}%
      \hskip-10mm\includesvg{figs/}{flow-graph_dot}
    \end{center}
  \end{figure}
  \end{columns}
\end{frame}

\begin{frame}{Exemplary image pairs}
  \begin{figure}
  \setlength{\hpwidth}{0.3\textwidth}
  \begin{center}
    \fbox{\scalebar[white]{\includegraphics[width=\hpwidth]{figs/000936_K2_0070_s00_08.png}}{800}{0.378}{1000}}\hfill% alv. dia. ~ 100 um, PS: 3000/1000*0.88um, 1/PS~ 0.378
    \fbox{\includegraphics[width=\hpwidth]{figs/000937_K2_0070_s00_09.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/000936-000937.png}}\\[2mm]
    \fbox{\scalebar[white]{\includegraphics[width=\hpwidth]{figs/001349_K2_0098_s00_01.png}}{800}{0.378}{1000}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001350_K2_0098_s00_02.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001349-001350.png}}\\[2mm]
  \end{center}
  \label{fig:pairs}
  \footnotesize
  \RaggedRight
  Left column: fixed image, middle column: moving image,\\
  right column: Magenta-Green overlay of the image pair.
  \end{figure}
\end{frame}

\begin{frame}{Metric values with markers for mIT and iPF}
  \begin{figure}[tbh]
  \setlength{\hpwidth}{\textwidth}
  \scriptsize
  \sffamily
  \def\svgwidth{\hpwidth}%
  \includesvg{figs/}{recRegStack}
  \label{fig:metrics}
  %% The point densities of mITs and iPFs are visualized with kernel density plots on the negative y-axis (unrelated to metric value, $\sigma = 10$).
  %% Mean of metric:    $\approx 4100$,
  %% Std. Dev.:         $\approx 9300$,
  %% some values are outside of the plot range,
  %% iPFs (52) are needed less often then mITs (621), mostly in cases of high metric values.
  %% The largest interval without any manual intervention (no mITs) is from 305 to 393 % K2_fibrosis/CZIto3D/czi2stack/slides/ord$ ls *.txt | grep -v pf | grep -o '^[0-9]*' > mIT.lst ; echo '[x, ix] = max(diff(load("mIT.lst"))); disp(x), disp(ix)' | octave -q 2>/dev/null ; sed -n 116p mIT.lst ; sed -n 117p mIT.lst
  %% even though the default parameter file (dPF) was tuned at different locations (e.g. slice 929, 1266 and 1379). % corresponding to slide 70, 96 and 100: ls *_K2_0070_s00_01.png ; git show 5d37b0d70a690dde0de6b2cefd0a9ff663232669:./Makefile | grep recRegStack.py ; ls *_K2_0100_s00_01.png ; git show 7e59379d3f206dfc78426b1cc6bd5e36a20ee24e:./Makefile | grep recRegStack.py
  %% The centre xz- and yz-slice of the result stack (as shown in Fig.~\ref{fig:VR}) are plotted for comparison. Distortions due to alignment drift can be seen, especially in the xz-slice up to slice 500.
\end{figure}
\end{frame}

\begin{frame}{Volume rendering of the 3D stack}
  \begin{figure}
    \setlength{\hpwidth}{\textwidth}
    \includegraphics[width=\hpwidth]{figs/K2_reg_smROI.png}%
  \end{figure}
  \footnotesize
  Volume rendering of the reconstructed 3rd~dimension of the lung tissue block, \\
  sub-extent of 880x880x2300~µm$^3$ (1000x1000x2607 voxel, ca. 44 GB @ 16-bit): \\
  Tissue dark, resin semi-transparent grey (airspaces and blood vessels).
%  Variations in image quality (staining, contrast, defocus) are visible in an interactive rendering.
\end{frame}

\begin{frame}{Registration based on distance map of binarization}
  \begin{figure}
  \setlength{\hpwidth}{0.3\textwidth}
  \begin{center}
    \fbox{\scalebar[black]{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_02.png}}{800}{0.378}{1000}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_03.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/02-03.png}}\\[2mm]
    \fbox{\scalebar[black]{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_03_fIod_BWR.png}}{800}{0.378}{1000}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_03_mIod_BWR.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/02-03_ot+dm.png}}\\[2mm]
  \end{center}
  \label{fig:pairs}
  \vspace{-1mm}
  \footnotesize
  \RaggedRight
  Top row: image pair as is (no mIT or iPF needed), fixed image, moving image, Magenta-Green overlay.
  Bottom row: corresponding ot+dm image pair (blue-white-red LUT from -50 to 50, blue: air, blood space; red: tissue, dirt).
  \end{figure}
\end{frame}

\begin{frame}{Thanks}
  for coding, support, maintenance and help go to:
  
  \begin{itemize}
  \item Susanne Faßbender
  \item Lena Ziemann
  \item Kasper Marstal% https://github.com/kaspermarstal
  \item Marius Staring% https://github.com/mstaring
  \item Fabien Pertuy% https://github.com/PertuyF
  \item Stefan Klein% https://github.com/stefanklein
  \item David Mastronarde
  \item Oleg Lobachev
  \item Elastix and ITK community
  \end{itemize}
  \vspace{-50mm}

  \begin{figure}
    \setlength{\hpwidth}{0.6\textwidth}
    \hfill
    \includegraphics[width=\hpwidth]{figs/K2_reg_smROI.png}%
  \end{figure}
\end{frame}

\begin{frame}{References}
  \RaggedRight% https://tex.stackexchange.com/questions/403429/force-linebreak-in-url-with-beamer-biblatex-and-biber#403454
  \printbibliography
\end{frame}


\end{document}
