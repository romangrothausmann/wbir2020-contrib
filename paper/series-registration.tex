\documentclass{llncs}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern, textcomp}% http://www.tug.dk/FontCatalogue/lmodern/
\usepackage[
  style= lncs,
  eprint=false,
  url=false,
  backend= biber
]{biblatex}
\addbibresource{series-registration.bib}
\usepackage{color}
\definecolor{dgreen}{rgb}{0.0,.5,0.0}
\definecolor{dblue}{rgb}{0.0,0.0,.5}
\usepackage[
  colorlinks= true,
  linkcolor= blue,
  citecolor= dgreen,
  urlcolor= dblue,
]{hyperref}

\usepackage{calc}% subtraction of lengths


%***********include SVG-files ;-)))
\usepackage{graphicx}      %for PDF inclusion
\usepackage{transparent} %for transparent text in inkscape files
\usepackage{import}      %for svg commands

\newcommand{\executeiffilenewer}[3]{%
  \ifnum\pdfstrcmp{\pdffilemoddate{#1}}%
  {\pdffilemoddate{#2}}>0%
  {\immediate\write18{#3}}\fi%
}
\newcommand{\includesvg}[2]{%
  \executeiffilenewer{#1/#2.svg}{#1/#2.pdf_tex}%  
  {inkscape -z --file=#1/#2.svg --export-pdf=#1/#2.pdf --export-latex --export-area-page}%--export-area-drawing
  \import{#1/}{#2.pdf_tex}%
}%
%***********include SVG-files ;-)))


\newlength{\hpwidth}
\newlength{\hswidth}
\newlength{\vpwidth}
\newlength{\vswidth}

\sloppy

\begin{document}

\title{Enabling manual intervention for otherwise automated registration of large image series}
\author{Roman Grothausmann\inst{1}\orcidID{0000-0001-5550-4239} \and Dženan Zukić\inst{2}\orcidID{0000-0002-9546-9703
} \and Matt McCormick\inst{2}\orcidID{0000-0001-9475-3756} \and Christian Mühlfeld\inst{1} \and Lars Knudsen\inst{1}}
\authorrunning{R. Grothausmann et al.}
\institute{
  Institute of Functional and Applied Anatomy,\\
  Hannover Medical School, Hannover, Germany\\
  and Biomedical Research in Endstage and Obstructive Lung Disease Hannover (BREATH)
  \and
  Kitware, Inc, USA
}
\maketitle
%
\begin{abstract}
  Aligning thousands of images from serial imaging techniques can be a cumbersome task. Methods (\cite{Arganda-Carreras2010, Klein2010, Mueller2011}) and programs for automation exist (e.g. \cite{fiji:virtual-stack-reg, elastix, histolozee}) but often need case-specific tuning of many meta-parameters (e.g. mask, pyramid-scales, denoise, transform-type, method/metric, optimizer and its parameters). Other programs, that apparently only depend on a few parameter often just hide many of the remaining ones (initialized with default values), often cannot handle challenging cases satisfactorily.
  
  Instead of spending much time on the search for suitable meta-parameters that yield a usable result for the complete image series, the described approach allows to intervene by manually aligning problematic image pairs. The manually found transform is then used by the automatic alignment as an initial transformation that is then optimized as in the pure automatic case. Therefore the manual alignment does not have to be very precise.
  This way the worst case time consumption is limited and can be estimated (manual alignment of the whole series) in contrast to tuning of meta-parameters of pure auto-alignment of complete series which can hardly be guessed.
%  This introduces a trade-off concerning the use of given time: either on tuning meta parameters in order to improve unsupervised auto alignment or on simple but repetitive initial manual alignment of failed cases of the unsupervised auto alignment.
\end{abstract}



\section{Introduction}

The general approach to reconstruct 3D by 2D serial sections (also termed array tomography) is long known and can be applied with various imaging techniques.\cite{Arganda-Carreras2010, Wang2015, Grothausmann2016b_r, Ochs2016_r} This method has the common drawback that the images of the serial sections need to be aligned to the image of the adjacent slice. While this can be done manually with various programs (e.g. midas of IMOD, Fiji/ImageJ, VV, Gimp, PhotoShop), this can be very tedious labour. Although visual inspection seems easy, it often is hard to decide which transform is the ``best'', one reason being the fact that adjacent images in general contain similar but not equal content due to the structure change in the 3rd dimension. This becomes of particular importance when employing registration allowing local deformations, because the natural 3D structure change is not meant to be corrected by local deformation.

Therefore, various procedures for digital automatic alignment have been investigated, which in general are based on finding a transformation that optimizes a metric (a well defined quantification in contrast to visually ``best''). Many types of transformations, metrics and optimizers have been developed of which specific ones need to be chosen depending on the given data and desired results. Apart from the parameters of the transformation that get optimized during the processing,  parameters of the chosen optimizer, metric and general ones such as denoising, size/shape of a mask and pyramid resolutions need to be set before the processing can start.\cite{Yoo2004, elastix, fiji:virtual-stack-reg} These parameters are referred to as meta-parameters and need to be tuned with expert knowledge in order to get an acceptable results for as many consecutive images as possible. The more serial sections the image series contains, the more difficult and time consuming this task can become. Experience shows, that for a series of a few hundred up to a few thousand realistic (i.e. non-ideal) images, the finding of suitable meta-parameters can take a few weeks, without a guarantee to succeed at all.

In order to have a better guarantee to succeed in practice, the procedure described in this paper limits the time consumption to that needed for a pure manual alignment of the whole series, while trying to use automation as much as possible.


\section{Method}

\begin{figure}[tbh]
  \setlength{\hpwidth}{0.4\textwidth}
  \begin{minipage}[c]{\hpwidth}
    \vspace{0cm}% essential for [t] to work
    \begin{center}
      \small%
      \sffamily
      \hypersetup{linkcolor=black}
      \def\svgwidth{\hpwidth}%
      \includesvg{figs/}{flow-graph_dot}
    \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}[c]{\textwidth-\hpwidth-5mm}
    \footnotesize
    Schematic flow graph to visualize the dependencies involved in the iterative process.
    Images are represented by squares, text-files by ovals.
    The parameters used by default (dfl.~PF, dPF) during the registration (reg.) need to be tuned for the integral image series (2D~series) in order to reduce the need for manual interactions.
    The first image at the start is copied unchanged. The last aligned image (ali.~Img) is used as fixed image (fix.~Img, fI) for registering the next image from the series (mov.~Img, mI).
    In the case that the default parameters do not yield an acceptable result for an individual image pair, it is possible to supply a manual initial transform (man.~iT, mIT) and/or provide individual registration parameters (idv.~PF, iPF).
    Tuning the default parameters (dPF) is the most difficult (time consuming, red) task, adjusting some individual parameters less problematic (iPF, yellow) while creating a manual initial transform (mIT, green) with e.g. \texttt{midas} is easiest.
    %      After each manual interaction it is possible to continue the registration process where it was stopped.
    %      For protocolling and reproducibility checks, it is possible to govern the text-files with \texttt{git}, the input image series and the output with \texttt{git-annex}.
    In case some images need to be re-scanned (due to scan-artefacts, defocus, etc), the transform parameter files (tra.~PF, tPF) can be used to register the new image exactly the same way (reprod.) or the registration process can be re-initiated to make use of the improved image quality.
  \end{minipage}
  \caption{Processing dependencies}
  \label{fig:fGraph}
\end{figure}

First Elastix~\cite{elastix}, later SimpleElastix~\cite{simpleelastix} was chosen as the framework that provides the means for automated registration. In general other implementation could be chosen, however Elastix (based on ITK~\cite{itk}) already accepts initial transformations. Even if an initial transformation is provided by manual alignment, it can still enter the automatic optimization and therefore get improved quantitatively as in the default case of pure automated optimization.
In other words, if the automated optimization gets trapped in a local optimum i.e. fails to find the global optimum, a manual initial transform provides a different start point for the optimization such that the global optimum is reached.

The work presented here is based on three distinct pieces of software:

\begin{enumerate}
\item The Python-implemented registration program \texttt{recRegStack.py} which employs SimpleElastix.\footnote{\url{http://github.com/romangrothausmann/elastix_scripts/}}
\item Extra programs and commands needed to convert gigapixel slice scans from a Carl Zeiss slide scanner (in CZI fromat) to an image series usable by \texttt{recRegStack.py}.
\item A build and invocation system to apply these to a full-size image series, with adjustments needed for the specific data at hand, using gnuplot\cite{gnuplot} and GNU~Parallel\cite{parallel}.\footnote{\url{http://github.com/romangrothausmann/CZIto3D}}
\end{enumerate}

\texttt{recRegStack.py} takes an Elastix/ITK parameter file (containing the definition of various meta-parameters) into account, which allows changing the default values used by SimpleElastix. The last transformed image is used to register the following one, see Fig.~\ref{fig:fGraph}.

For the proof of principle, \texttt{midas} of the IMOD package~\cite{imod} was chosen for manual alignment due to its superior precision and interaction possibilities.
% \texttt{make} governs the conversion of the input image pair to the MRC-format (\texttt{midas} necessity, employing \texttt{tif2mrc} from IMOD) and the conversion of the IMOD transform file to Elastix format.
Manually created initial transform files (mITs, Fig.~\ref{fig:fGraph}) will then be taken into account by \texttt{recRegStack.py} when continuing the automated alignment.
In addition, it is possible to adjust the meta-parameters for individual image pairs (iPF, Fig.~\ref{fig:fGraph}) in case the mIT together with the global defaults (dPF, Fig.~\ref{fig:fGraph}) do not lead to a satisfactory result.%
\footnote{The iPF also allows to suppress further auto adjustments of an mIT in case the global optimum does not represent the correct transformation, which can happen for very destorted slices with repetitive, similar structures.} % gitk -- `grep -l '^(MaximumNumberOfIterations' *pf.txt
This can happen for example if the fixed image (fI) and the moving image (mI) come from different section bands, possibly differing significantly in focus quality.

%%%% implied by use described in sec:res
%% Apart from the alignment, it is also possible to modify the order of the images in case the natural order of successive slices got lost due to occasional sample preparation difficulties.
%% This order list and the manually found transforms can easily be managed by \texttt{git} which also offers a very convenient way to highlight possible changes needed to achieve the final result. This way all manual adjustments are stored in a git-repository and can be published for automated reproducibility.


\section{Application and Results}

\begin{figure}[tbh]
  \begin{center}
  \begin{tabular}{c|c|c|c|c}
    \scalebox{0.7}{\rotatebox{90}{\includegraphics{figs/K2_0008_s02.png}}} &
    \scalebox{0.7}{\rotatebox{90}{\includegraphics{figs/K2_0006_s00.png}}} &
    \scalebox{0.7}{\rotatebox{90}{\includegraphics{figs/K2_0105_s05.png}}} &
    \scalebox{0.7}{\rotatebox{90}{\includegraphics{figs/K2_0026_s00.png}}} &
    \scalebox{0.7}{\rotatebox{90}{\includegraphics{figs/K2_0105_s02.png}}}
  \end{tabular}
  \end{center}
  \caption{Bands of serial sections of stained lung tissue on glass slides}
  \label{fig:slices}
  Some exemplary thumbnails of slide scans with bands of serial sections of lung tissue stained on glass slides. Ranging from good (left, one band well aligned and no significant staining variations) to bad (right, broken bands with unobvious order, staining variations and slice loss due to folds and extending slide border).
  Lines indicate different glass slide.
  \footnotesize
\end{figure}

The described approach was applied to an image series of about 2600 histological serial section of lung tissue (rat, details can be found in~\cite{Steffen2017}), referred to as K2-dataset.
An EM~UC7 microtome (Leica, Germany) was used to cut semi-thin sections with a thickness of 1~µm connected to bands of about 1 to 20 sections. These bands were placed on 177 glass slides (where possible as a single row) see Fig.~\ref{fig:slices}. After staining with toluidine blue, the slides were digitalized at a magnification of 10X by an AxioScan~Z1 (Zeiss, Germany) using a single-channel fluorescence camera with a very low transmission light in order to get greyscale images (in CZI format) with a dynamic range above 8-bits.

The build and invocation system for this image series can be found in \url{http://gitlab.com/romangrothausmann/K2_fibrosis/}. This git repository holds references to the raw-data (CZIs) in an annex (\url{https://git-annex.branchable.com/}), imports \url{http://github.com/romangrothausmann/CZIto3D} as a subtree for local adjustments as needed for the specific data and serves as processing protocol.
\texttt{recRegStack.py} from \url{http://github.com/romangrothausmann/elastix_scripts/} is invoked via a docker-image (\url{http://www.docker.com/}) containing all the needed libraries to reproducibly register the images. There is a short (down-scaled) image series for testing in \href{http://github.com/romangrothausmann/elastix_scripts/tree/ba86940716bacfed96641a48a39053014519cb54/tests/recRegStack}{tests/recRegStack/}.


\begin{figure}
  \setlength{\hpwidth}{0.3\textwidth}
  \begin{center}
    \fbox{\includegraphics[width=\hpwidth]{figs/000936_K2_0070_s00_08.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/000937_K2_0070_s00_09.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/000936-000937.png}}\\[2mm]
    \fbox{\includegraphics[width=\hpwidth]{figs/001349_K2_0098_s00_01.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001350_K2_0098_s00_02.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001349-001350.png}}\\[2mm]
    \fbox{\includegraphics[width=\hpwidth]{figs/001348_K2_0097_s00_15.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001349_K2_0098_s00_01.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001348-001349.png}}\\[2mm]
    \fbox{\includegraphics[width=\hpwidth]{figs/001355_K2_0098_s01_01.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001356_K2_0098_s01_02.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/001355-001356.png}}\\[2mm]
  \end{center}
  \caption{Exemplary image pairs}
  \label{fig:pairs}
  \footnotesize
  Left column: fixed image, middle column: moving image, right column: Magenta-Green overlay of the image pair (similar to midas).
  Image pair rows:
  \begin{enumerate}
    \item ideal (no mIT or iPF needed). % egrep ' 000936| 000937' recRegStack.out
    \item \label{l:dirt} dirt (mIT but no iPF needed). % egrep ' 001349| 001350' recRegStack.out
    \item defocus (no mIT or iPF needed). % egrep ' 001348| 001349' recRegStack.out
    \item folds (no mIT or iPF needed). % egrep ' 001355| 001356' recRegStack.out
  \end{enumerate}
  Metric for \ref{l:dirt} without mIT is 34974 % rm ../001350_K2_0098_s00_02.txt 001349_K2_0098_s00_01.tif 001350_K2_0098_s00_02.tif; docker run -t --rm --user `id -u`:`id -g` -v $(pwd)/../../:/images -w /images/CZIto3D/czi2stack/slides/ord/ registry.gitlab.com/romangrothausmann/elastix_scripts/master:34cb4e3c recRegStack.py -i "*.png" -o reg/ -p ../../parameterFile.txt -m 1000 2000 1000 2000 -n 32 -s 001349_K2_0098_s00_01.png  -f -b
  and with mIT 20214.
\end{figure}

\begin{figure}[tbh]
  \setlength{\hpwidth}{\textwidth}
  \scriptsize
  \sffamily
  \def\svgwidth{\hpwidth}%
  \includesvg{figs/}{recRegStack}
  \vspace{3mm}
  \rmfamily
  \caption{Plot of metric values with markers for mIT and iPF}
  \label{fig:metrics}
  \footnotesize
  %% from slides/ord/recRegStack.stat
  The point densities of mITs and iPFs are visualized with kernel density plots on the negative y-axis (unrelated to metric value, $\sigma = 10$).
  Mean of metric:    $\approx 4100$,
  Std. Dev.:         $\approx 9300$,
  some values are outside of the plot range,
  iPFs (52) are needed less often then mITs (621), mostly in cases of high metric values.
  The largest interval without any manual intervention (no mITs) is from 305 to 393 % K2_fibrosis/CZIto3D/czi2stack/slides/ord$ ls *.txt | grep -v pf | grep -o '^[0-9]*' > mIT.lst ; echo '[x, ix] = max(diff(load("mIT.lst"))); disp(x), disp(ix)' | octave -q 2>/dev/null ; sed -n 116p mIT.lst ; sed -n 117p mIT.lst
  even though the default parameter file (dPF) was tuned at different locations (e.g. slice 929, 1266 and 1379). % corresponding to slide 70, 96 and 100: ls *_K2_0070_s00_01.png ; git show 5d37b0d70a690dde0de6b2cefd0a9ff663232669:./Makefile | grep recRegStack.py ; ls *_K2_0100_s00_01.png ; git show 7e59379d3f206dfc78426b1cc6bd5e36a20ee24e:./Makefile | grep recRegStack.py
  The centre xz- and yz-slice of the result stack (as shown in Fig.~\ref{fig:VR}) are plotted for comparison. Distortions due to alignment drift can be seen, especially in the xz-slice up to slice 500.
\end{figure}

Fiji~\cite{fiji} was used to roughly mark the centre of each section in thumbnail images of the gigapixel scans (\href{http://github.com/romangrothausmann/CZIto3D/blob/4da67edd107303b23ed00a6230cc38775a3b9007/czi2stack/Makefile#L96-L104}{\texttt{czi2stack/Makefile}}).
These centres were then used to automatically extract the region of each slice as its own image (3000~x~3000 pixel, PNG) with bfconvert~\cite{bftools} (\href{http://github.com/romangrothausmann/CZIto3D/blob/4da67edd107303b23ed00a6230cc38775a3b9007/czi2stack/Makefile#L179-L192}{\texttt{czi2stack/Makefile}}).
In case of broken bands, the ordering implied by the centre marks had to be adjusted to match the physical order (\href{http://gitlab.com/romangrothausmann/K2_fibrosis/blob/ba87b312a8047e58dc0fcd0ebcdd000465c902fe/morph-fib/czi2stack/slides/slideOrder.lst}{\texttt{czi2stack/slides/slideOrder.lst}}, discrepancies often only visible after a registration).
This order was then used to register the consecutive slices (\href{http://gitlab.com/romangrothausmann/k2_fibrosis/blob/ba87b312a8047e58dc0fcd0ebcdd000465c902fe/morph-fib/czi2stack/Makefile#L111-115}{\texttt{czi2stack/Makefile}}).
The mask for registration and the default parameter file (dPF) were adjusted to fit the K2-dataset, % (see e.g. \href{http://gitlab.com/romangrothausmann/k2_fibrosis/commit/bfec8372872af28aa879fc64d78dc4353ea447ee}{\texttt{K2\_fibrosis@bfec8372}}).
applying rigid registration using a "MultiResolutionRegistration" with "AdvancedMeanSquares" metric and  "AdaptiveStochasticGradientDescent" as optimizer, see \href{http://gitlab.com/romangrothausmann/k2_fibrosis/blob/ba87b312a8047e58dc0fcd0ebcdd000465c902fe/morph-fib/czi2stack/parameterFile.txt}{\texttt{czi2stack/parameterFile.txt}} for details.
Still,
621 mITs (in average every 4th image) % ls -1 slides/ord/*txt | grep -v pf.txt | wc -l
and 52 iPFs (in average every 50th image) % ls -1 slides/ord/*txt | grep pf.txt | wc -l
were needed to align all 2607 images, see Fig.~\ref{fig:metrics}.
Some exemplary image pairs (good, dirt, defocus, folds) are shown in Fig.~\ref{fig:pairs}.
Since the registration reconstructs the spatial correspondence in the 3rd dimension, the resulting image stack can then be regarded as a 3D dataset of 3000~x~3000~x~2607 voxel (about 44~GB~@~16-bit), see Fig.~\ref{fig:VR}.

\begin{figure}
  \setlength{\hpwidth}{\textwidth}
  \includegraphics[width=\hpwidth]{figs/K2_reg_smROI.png}%
  \caption{Volume rendering of the 3D stack}
  \label{fig:VR}
  \footnotesize
  Volume rendered visualization of the reconstructed 3rd~dimension of the lung tissue block (sub-extent of 1000x1000x2607 voxel).
  Tissue dark, resin semi-transparent grey (airspaces and blood vessels).
%  Variations in image quality (staining, contrast, defocus) are visible in an interactive rendering.
\end{figure}


The volume in lung samples occupied by tissue is only about 10\%~-~20\%~\cite{Grothausmann2016b_r, Buchacker2019_r}, so there is about 90\%~-~80\% mostly non-correlating texture in image pairs which disturbs the registration process. This is one reason why the registration of serial sections of lung tissue is challenging.
A possible countermeasure is to ``fill'' the non-tissue space with (roughly) correlating data. This can be achieved by first auto-thresholding the image to roughly binarize tissue and non-tissue and then generating a distance map, which is implemented in the branch \href{http://github.com/romangrothausmann/elastix_scripts/tree/ot+dm}{ot+dm} and leads to image pairs as in Fig.~\ref{fig:ot+dm}.
However, applying this version of \texttt{recRegStack.py} to the K2-dataset (\href{http://gitlab.com/romangrothausmann/k2_fibrosis/commit/a5617581eb7b50acf443dd57b72637dcdcb3b923}{\texttt{K2\_fibrosis@a5617581}}) showed, that more mITs are needed.\footnote{A full alignment with ot+dm was not pursued further because currently SimpleITK of SimpleElastix does not allow to set a mask for the otsu-threshold calculation (\href{http://github.com/romangrothausmann/elastix_scripts/tree/ot-mask}{ot-mask}). Therefore, the continuation feature of \texttt{recRegStack.py} cannot be used so that after each mIT creation the registration has to start from the beginning and therefore the whole procedure needs much more time. An alternative would be to port \texttt{recRegStack.py} to \href{http://github.com/InsightSoftwareConsortium/ITKElastix}{ITKElastix}.} A reason for this might be that this approach is more sensitive to dirt, which ends up in the tissue segment and therefore causes significant disturbance in the distance transform, see Fig.~\ref{fig:ot+dm}.
Another promising approach could be registration based on landmarks generated by SIFT~\cite{Lowe1999,Cheung2007}, similar to Fiji's ``Register Virtual Stack Slices''~\cite{fiji:virtual-stack-reg} but using Elastix/ITK in order to keep the features of manual intervention (mIT and iPF)\footnote{The lack of the possibility for manual intervention and a trial of more than two weeks to find meta-parameters to register the K2-dataset with Fiji's ``Register Virtual Stack Slices'' was actually the motivation for implementing \texttt{recRegStack.py}}.

\begin{figure}[tbh]
  \setlength{\hpwidth}{0.3\textwidth}
  \begin{center}
    \fbox{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_02.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_03.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/02-03.png}}\\[2mm]
    \fbox{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_03_fIod_BWR.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/2016_12_02_0101_s00_03_mIod_BWR.png}}\hfill
    \fbox{\includegraphics[width=\hpwidth]{figs/02-03_ot+dm.png}}\\[2mm]
  \end{center}
  \caption{Exemplary image pair for ot+dm variant}
  \label{fig:ot+dm}
  \footnotesize
  Top row: image pair as is (no mIT or iPF needed), fixed image, moving image, Magenta-Green overlay.
  Bottom row: ot+dm image pair for images on the left (blue-white-red LUT from -50 to 50, blue: air, blood space; red: tissue, dirt).
\end{figure}


\section{Discussion}

The described proof of principle combines automated alignment with manual intervention such that ideally the automation does the whole work but also ensures that ``in the worst case'' at least the result of a pure manual alignment will be achieved.
This comes with a need to balance the two time consuming tasks:
Either tuning the meta-parameters for the automated alignment of as many images in the series as possible (total time consumption can be unlimited)
or helping the automation with initial manual alignments (total time is limited).

While the current implementation with Makefiles serves as a proof of principle, it can be further improved to a more intuitive and user-friendly program. For example, by incorporating the processing done by various commands into the Python code, as well as avoiding \texttt{midas} and other IMOD-tools or aborting auto alignment in order to provide a manual alignment to continue with.
A graphical user interface (GUI) could provide visual feedback (similar to the image viewer \texttt{geeqie}), incorporate the needed \texttt{midas} functionality and offer a ``manual intervention'' button.
In principle, a threshold on the final metric value could be used to automatically trigger the suggestion of a manual alignment.

Another variant of \texttt{recRegStack.py} (branch \href{http://github.com/romangrothausmann/elastix_scripts/tree/combT_01}{combT\_01}) %\footnote{Demanded improvements \url{http://github.com/SuperElastix/SimpleElastix/issues/102}}
accumulates all former transforms and adds the newly found transform of the processed image pair as in \cite{Mueller2011}.
% In terms of IMOD/\texttt{midas}~\cite{imod}, this can be regarded as finding local transforms while the latter method would correspond to global transforms.
While the two approaches should yield similar results, accumulation of a few hundred transforms can become problematic but in return can avoid larger differences in case of already similarly recorded image pairs.
A third variant (branch \href{http://github.com/romangrothausmann/elastix_scripts/tree/reg2tra+prevTra}{reg2tra+prevTra}) uses both approaches and chooses the result with the better metric value finally achieved.

Ideally, a (non-destructive) tomogram or some form of markers should be used for guiding 3D reconstruction of serial sections (e.g. constrain local deformation corrections and avoid continuous drift), such functionality is offered by e.g. HistoloZee\cite{histolozee}. 

\texttt{recRegStack.py} provides the option to ignore some images of the series in case some slices are too distorted for registration or lost during the preparation (\href{https://gitlab.com/romangrothausmann/k2_fibrosis/blob/ba87b312a8047e58dc0fcd0ebcdd000465c902fe/morph-fib/czi2stack/lostSlieds.lst}{\texttt{czi2stack/lostSlieds.lst}}). After aligning the next usable image of the series, a reconstruction of the lost slice can be created from the adjacent slices.\cite{Lobachev2020}


\section{Acknowledgement}

Special thanks go to
Susanne Faßbender (for excellent technical assistance),
Lena Ziemann (for slide scanning and creation of mITs and iPFs),
Kasper Marstal, Fabien Pertuy, Stefan Klein and Marius Staring (for feedback on use of Elastix, SimpleElastix and debugging)
and to David \mbox{Mastronarde}, Daniel Adler (for feedback on IMOD and HistoloZee details)
and Oleg Lobachev (for feedback on the general approach).

\printbibliography

\end{document}
